module.exports = function(grunt) {

    require('time-grunt')(grunt);
    var path = require('path');

    require('load-grunt-config')(grunt, {
        configPath: [
            path.join(process.cwd(), 'vendor/pizone/admin-bundle/Resources/config/grunt'),
            path.join(process.cwd(), 'vendor/pizone/catalog-bundle/Resources/config/grunt'),
            path.join(process.cwd(), 'vendor/pizone/content-bundle/Resources/config/grunt'),
            path.join(process.cwd(), 'vendor/pizone/user-bundle/Resources/config/grunt'),
            path.join(process.cwd(), 'vendor/pizone/form-bundle/Resources/config/grunt'),
            path.join(process.cwd(), 'app/config/grunt')
        ],
        jitGrunt: true,
        // staticMappings: {
        //     jshint: 'web/bundles/pizoneadmin/grunt/jshint.js'
        // }
    });
};