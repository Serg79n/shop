var CartService = function(){
    var self = this;
    self.cart = {};
    self.count = 0;
    self.block = null;
    self.cartName = 'wf_cart';
    self.price = {};
    self.itog = null;
    self.goods = null;

    self.AddToCart = AddToCart;
    self.Remove = Remove;
    self.Main = Main;

    function Main() {
        var countFields =  $('.colorful');
        $.each(countFields, function(i, field){
            EventCountField(field);
        });

        self.itog = $('#itog .price');
        UpdateItog();
    }

    function EventCountField(field) {
        field = $(field);
        var btns = field.parent().find('button');
        var attrId = field.attr('id');
        var id = attrId.split('_')[1];
        self.price[id] = {
            cost: $('#price_' + id),
            end_cost: $('#end_price_' + id)
        };

        field.on('input', function(e){
            self.cart[id].count = $(this).val();

            UpdateCost(id);
        });

        $.each(btns, function(i, btn){
            $(btn).click(function(e){
                var action = $(e.target).text();
                if(action == '+'){
                    self.cart[id].count = self.cart[id].count + 1;
                }
                if(action == '-'){
                    if(self.cart[id].count > 1)
                        self.cart[id].count = self.cart[id].count - 1;
                }

                UpdateCost(id);
            });
        });

    }

    function UpdateCost(id){
        Cookies.set(self.cartName, self.cart, { path: '/' });

        self.price[id]['end_cost'].text(self.price[id].cost.text() * self.cart[id].count);

        UpdateItog();
        CheckCount();
        Indicator();
    }

    function UpdateItog(){
        var endCost = 0;
        for(var key in self.cart){
            endCost = endCost + parseInt(self.price[key]['end_cost'].text());
        }

        self.itog.text(endCost);
    }

    function Remove(id){
        if(self.cart && Object.keys(self.cart).length > 0){
            delete self.cart[id];

            Cookies.set(self.cartName, self.cart, { path: '/' });

            $('#goods_' + id).remove();
        }

        return false;
    }
    
    function AddToCart(id){
        self.count = 0;
        if(self.cart && Object.keys(self.cart).length > 0){
            if(self.cart.hasOwnProperty(id)){
                self.cart[id].count = self.cart[id].count + 1;
            }
            else{
                self.cart[id] = {
                    id: id,
                    count: 1
                };
            }
        }
        else{
            self.cart[id] = {
                id: id,
                count: 1
            };
        }

        Cookies.set(self.cartName, self.cart, { path: '/' });

        CheckCount();
        Indicator();
        CheckInCart();

        // swal("Корзина", "Товар успешно добавлен в корзину!", "success")

        return false;
    }

    function CheckCount(){
        for(var key in self.cart){
            self.count = self.count + self.cart[key].count;
        }
    }

    function Indicator(){
        if(self.count > 0)
            self.block.removeClass('disabled');
        else
            self.block.addClass('disabled');
    }

    function CheckInCart(){
        self.goods.removeClass('active').text('В корзину');

        for(var key in self.cart){
            $('#goods_' + key + ' .in_cart').addClass('active').text('Товар в корзине');
        }
    }

    function Init(){
        var cart = Cookies.getJSON(self.cartName);
        self.block = $('#cartInfo');

        if(cart)
            self.cart = cart;

        self.goods = $('.in_cart');

        CheckCount();
        Indicator();
        CheckInCart();
    }

    Init();
};

var Cart = new CartService();