function GoodsListCtrl($scope, $http){
    $scope.goodsList = [];
    $scope.alias = '';
    $scope.filters  = {};
    $scope.sortBy = '';
    var popup = $('<div class="resultFind"><img src="/bundles/pizonebackend/img/ajax-loader.gif"> Ищем ...</div>');
    $scope.lastPopup= null;
    $scope.filterForm = null;
    $scope.testInitFilters = false;
    $scope.testInitSortBy = false;
    $scope.count = '';

    $scope.Click = {
        ClearFilter: ClickClearFilter
    };

    $scope.$watch('filters', function(oldVal, newVal){
        if($scope.testInitFilters) {
            var diff = DeepDiffMapper.map(oldVal, newVal);

            function GetDifKey(diff) {
                for (var key in diff) {
                    if (diff[key].type != "unchanged")
                        return key;
                }
            }

            var id = GetDifKey(diff);
            var target = $('#params_' + id);
            Filter(target);
        }
        else
            $scope.testInitFilters = true;
    }, true);


    function Filter($target){
        hidePopUp();
        $scope.lastPopup = popup;
        $scope.filterForm = $('#filterform');
        $scope.filterForm.append($scope.lastPopup);

        if($('body').hasClass('mobile'))
            $('.resultFind').css('margin-top', 0);

        if($target)
            filterPopUpRender($target);


        if($('body').hasClass('mobile'))
            $('.resultFind').css('margin-top', '20px');

        $.ajax({
            url: GetUrl(GetFilterParameters())
        })
            .done(function(data,req){
                $scope.lastPopup.html("Найдено: " + data.count + ". <span>Показать</span>")
                $scope.lastPopup.click(function(){
                    window.location.href = GetUrl(GetFilterParameters());
                })
            });
    }

    function hidePopUp() {
        if ($scope.lastPopup)
            $scope.lastPopup.remove();
    }

    function filterPopUpRender($target) {
        var $label  = $($target.closest('label'));
        $scope.lastPopup.offset($.extend($label.position(), {left: '50%'}, {}))
    }


    function ClickClearFilter(){
        $scope.testInitFilters = false;
        $scope.testInitSortBy = false;
        $.each($scope.filters, function(i){
            var check = $('#params_' + i);
            check[0].checked = false;
            check.next('span').removeClass('checked');
        });
        $scope.sortBy = '';
        $scope.filters = {};

        window.location.href = window.location.pathname;
    }

    function GetFilterParameters(){
        var params = [],
            result = {};
        $.each($scope.filters, function(i, param){
            if(param)
                params.push(i);
        });
        result['parameters'] = params.join(',');

        return result;
    }

    function Init(){
        var query = window.location.href;
        var qs = getUrlParams(query);
        if(qs.hasOwnProperty('parameters')) {
            var start = qs['parameters'].split(',');
            $.each(start, function (i, id) {
                $scope.filters[id] = true;
            });
        }
        $('#filterform').on('click','.disabled label',function(e){ e.preventDefault(); e.stopPropagation(); return false; });
    }

    Init();


    function getUrlParams(url) {
        var str = url.split("?");
        var keyValue, params = {};
        if(str[1]) {
            var queryString = url.split("?")[1];
            var keyValuePairs = queryString.split("&");
            keyValuePairs.forEach(function (pair) {
                keyValue = pair.split("=");
                params[keyValue[0]] = decodeURIComponent(keyValue[1]).replace("+", " ");
            });
        }
        return params
    }

    function GetUrl(dataQuery){
        var uri = window.location.pathname;
        var query = window.location.href;

        var qs = getUrlParams(query);

        for(var key in dataQuery){
            qs[key] = dataQuery[key];
        }

        var str = [];
        for(var i in qs){
            str.push(i + '=' + qs[i]);
        }

        return uri + (str ? '?' + str.join('&'): '');
    }
}

GoodsListCtrl.$inject = ['$scope', '$http'];