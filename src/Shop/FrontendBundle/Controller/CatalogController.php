<?php

namespace Shop\FrontendBundle\Controller;

use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter as PagerAdapter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Shop\FrontendBundle\Controller\ContentController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class CatalogController extends Controller {

    public function indexAction(Request $request) {
        $category = $this->get('pi_zone.catalog_category')->GetTreeCategoriesByAlias();

        if (!$category)
            throw new NotFoundHttpException();

        return $this->render(
            'ShopFrontendBundle:Catalog/Category:index.html.twig', array(
                'items' => $category['tree'],
                'parent' => $category['parent']
            )
        );
    }

    public function showAction($alias, $page, Request $request) {
        $category = $this->get('pi_zone.catalog_category')->findByAlias($alias);

        if (!$category)
            throw new NotFoundHttpException();

        if($category->getLevel() <= 1 && $category->getChildren()->count() > 0){
            return $this->render(
                'ShopFrontendBundle:Catalog/Category:category_list.html.twig', array(
                    'category' => $category,
                    'items' => $category->getChildren(),
                    'alias' => $alias
                )
            );
        }
        else{

            $orderBy = $request->query->get('order_by');
            $groupParameters = $request->query->get('parameters');

            $filterForm = $this->get('pi_zone.catalog_category')->getFilteringParameters($category);

            $filters = array();
            if($groupParameters) {
                $ids = explode(',', $groupParameters);
                $result = array();
                foreach($filterForm as $key1 => $one){
                    $selected = array();
                    foreach($one['parameters'] as $key2 => $i){
                        if(in_array($i['id'], $ids)) {
                            $selected[] = $i['id'];
                            $filterForm[$key1]['parameters'][$key2]['checked'] = true;
                        }
                    }
                    if($selected)
                        $result[$one['id']] = $selected;
                }
                $filters['group_parameters'] = $result;
            }

            $query = $this->get('pi_zone.catalog_category')->getChildContentByAliasQuery($alias, $orderBy, $filters);

            $content = new Pagerfanta(new PagerAdapter($query));
            $content->setMaxPerPage($this->getParameter('max_per_page'));
            $content->setCurrentPage($page, false, true);

            $isAjax = $request->isXMLHttpRequest();
            if ($isAjax) {
                return $this->json(array('count' => $content->getNbResults()));
            }
            else {
                return $this->render(
                    'ShopFrontendBundle:Catalog/Category:goods_list.html.twig', array(
                        'category' => $category,
                        'items' => $content,
                        'filters' => $filterForm,
                        'count' => count($content),
                        'alias' => $alias
                    )
                );
            }
        }
    }

    public function goodsAction($category, $alias){
        $content = $this->get('pi_zone.catalog_category')->getContentByAlias($alias);

        if ($content['result'] == 'error')
            throw new NotFoundHttpException($content['message']);

        return $this->render(
            'ShopFrontendBundle:Catalog:goods.html.twig', array(
                'alias' => $alias,
                'content' => $content['result']['item']
            )
        );
    }

    public function listAction(Request $request) {
        $catalog = $this->get('pi_zone.catalog')->getTree();

        if ($catalog['result'] == 'error')
            throw new NotFoundHttpException($catalog['message']);

        return $this->render(
                        'ShopFrontendBundle:Catalog:list.html.twig', array(
                    'content' => $catalog['result']['items']
                        )
        );
    }

    public function breadcrumbsAction($node = null) {
        if($node) {
            $crumbs = $this->get('pi_zone.catalog_category')->getPathForNode($node);

            return $this->render(
                'ShopFrontendBundle:Catalog/Category:_breadcrumbs.html.twig', array(
                    'items' => $crumbs,
//                    'node' => $node
                )
            );
        }
        else{
            return $this->render(
                'ShopFrontendBundle:Catalog/Category:_breadcrumbs.html.twig', array(
                    'items' => array(),
//                    'node' => $node
                )
            );
        }
    }

    public function leftMenuAction($alias) {
        $catalog = $this->get('pi_zone.catalog')->getMenu($alias);
        if ($catalog['result'] == 'error')
            throw new NotFoundHttpException($catalog['message']);

        return $this->render(
                        'ShopFrontendBundle:Catalog:_left_menu.html.twig', array(
                    'items' => $catalog['result']['items'],
                    'alias' => $alias
                        )
        );
    }

    public function topMenuAction($alias){
        $catalog = $this->get('pi_zone.catalog')->getTree(3);

        if ($catalog['result'] == 'error')
            throw new NotFoundHttpException($catalog['message']);
        return $this->render(
            'ShopFrontendBundle:Catalog:_top_menu.html.twig', array(
                'categories' => $catalog['result']['items'],
                'alias' => $alias
            )
        );
    }

    public function orderAction($catalog_alias, $alias){
        $em = $this->getDoctrine()->getRepository('ShopContentBundle:Content');
        $content = $em->GetContentByAlias($alias);

        if (!$content) {
            throw $this->createNotFoundException('Unable to find Content with alias = ['.$alias.']');
        }
        
        $catalog = $this->get('pi_zone.catalog')->getContentByAliasShort($catalog_alias);
        
        $content = $this->adaptationContent($content, $catalog);
        
        return $this->render(
            'ShopFrontendBundle:Content:index.html.twig', array(
                'content' => $content)
        );
    }

    public function searchAction(Request $request) {
        $query = $request->get('query');
        if ($query && trim($query) != ''){

            $page = $request->get('page');
            if(!$page)
                $page = 1;

            $catalog = $this->get('pi_zone.catalog')->search($query, $page);

            $pager = ceil($catalog['result']['count']/10);


            return $this->render(
                'ShopFrontendBundle:Search:list.html.twig', array(
                    'alias' => '',
                    'query' => $query,
                    'content' => addslashes(json_encode($catalog['result']['items'])),
                    'count' => $catalog['result']['count'],
                    'pager' => array('current' => $page, 'list' => $pager)
                )
            );
        }

        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }
    
    private function getParameters($one){
        $paramsArray = explode(',', $one);
        
        $result = array();
        foreach($paramsArray as $param){
            $t = explode(':', $param);
            $result[trim($t[0])] = trim($t[1]); 
        }
        
        return $result;
    }
    
    private function adaptationContent($content, $catalog){
        $content->setTitle(str_replace('{{title}}', $catalog['result']['item']['title'], $content->getTitle()));
        
        if(isset($catalog['result']['item']['article']))
            $content->setTitle(str_replace('{{article}}', $catalog['result']['item']['article'], $content->getTitle()));
        else
            $content->setTitle(str_replace('{{article}}', '', $content->getTitle()));
        
        $content->setContent(str_replace('{{title}}', $catalog['result']['item']['title'], $content->getContent()));
        if(isset($catalog['result']['item']['article']))
            $content->setContent(str_replace('{{article}}', $catalog['result']['item']['article'], $content->getContent()));
        else
            $content->setContent(str_replace('{{article}}', '', $content->getContent()));
        
        $pattern = preg_match_all('/{{form\((.*)\)}}/isU', $content->getContent(), $matches, PREG_SET_ORDER);

        foreach($matches as $one){
            $params = $this->getParameters($one[1]);
            
            $component = new ContentController();
            $component->setContainer($this->container);
             
            $content->setContent(str_replace($one[0], $component->getFormAction($params['alias'], str_replace(array('"', '\''), '', $params['redirect_url']))->getContent(), $content->getContent()));
        }
        
        return $content;
    }
}
