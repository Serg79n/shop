(function () {
    angular.module('PiZone', [
        'PiZone.Admin',
        'PiZone.Content',
        'PiZone.Catalog',
        'PiZone.User',
        'PiZone.Form'
    ])
        .run(['$state', function ($state) {
            console.log('test');
        }]);
})();

// Other libraries are loaded dynamically in the config.js file using the library ocLazyLoad