<?php

function tree_al_ns($pdo, $s_table, $i_value = 0, $k_parent = 0)
//обновляет вложенные множества в соответствие со списками смежности
//$s_table содержит в себе имя таблицы с деревом.
//  В этой таблице дерево хранится комбинированным способом:
//  списки смежности и вложенные множества
//$i_value - идентификатор первого свободного значения (левого или правого)
//$k_parent - идентификатор текущей родительской вершины.
//  0 - для корневой вершины
//возвращает следующее свободное значение или
//  false в случае ошибки
//пример вызова:
//  if(tree_al_ns('t_catalog_tree')===false)
//    echo 'Конвертирование прошло с ошибками.';
{
    $query = "select id from " . $s_table . " where parent_id " . ($k_parent ? ('='.$k_parent) : ' is null');
    $stmt = $pdo->prepare($query);
    $r = $stmt->execute();
    if (!$r)
        return false;

    $all = $stmt->fetchAll();
    for ($i = 0; $i < count($all); $i++) {
        $f = $all[$i];
        $k_item = $f[0];
        $i_right = tree_al_ns($pdo, $s_table, $i_value + 1, $k_item);
        if ($i_right === false) return false;
        if (!$pdo->query("
      update
        " . $s_table . "
      set
        lft=" . $i_value . ",
        rgt=" . $i_right . "
      where
        id=" . $k_item . "
      ")
        ) return false;
        $i_value = $i_right + 1;
    }
    return $i_value;
}

$pdo = new PDO(
    'mysql:host=localhost;dbname=catalog',
    'root',
    'fd78rw42');

tree_al_ns($pdo, 'catalog');
