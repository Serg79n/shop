module.exports = {

    // Опции
    options: {
        limit: 3
    },

    clear: [
        'clean'
    ],

    // Задачи разработки
    devFirst: [
        'clean',
        'jshint'
    ],
    devSecond: [
        'cssmin',
        'uglify'
    ],
    devTherd: [
        'copy'
    ],

    // Производственные задачи
    prodFirst: [
        'clean',
        'jshint'
    ],
    prodSecond: [
        'cssmin',
        'uglify'
    ],
    prodTherd: [
        'copy'
    ]
};