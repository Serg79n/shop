<?php

namespace Application\Migrations\Catalog;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170314183637 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE catalog_content');
        $this->addSql('DROP TABLE catalog_parameter');
        $this->addSql('DROP TABLE catalog_stock');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE catalog_content (content_id INT NOT NULL, catalog_id INT NOT NULL, INDEX IDX_F177CD8584A0A3ED (content_id), INDEX IDX_F177CD85CC3C66FC (catalog_id), PRIMARY KEY(content_id, catalog_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE catalog_parameter (catalog_id INT NOT NULL, parameter_id INT NOT NULL, INDEX IDX_D0AA7BADCC3C66FC (catalog_id), INDEX IDX_D0AA7BAD7C56DBD6 (parameter_id), PRIMARY KEY(catalog_id, parameter_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE catalog_stock (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }
}
