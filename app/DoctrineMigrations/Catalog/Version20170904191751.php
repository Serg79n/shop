<?php

namespace Application\Migrations\Catalog;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170904191751 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');


        $this->addSql('ALTER TABLE catalog ADD CONSTRAINT FK_1B2C324784A0A3ED FOREIGN KEY (content_id) REFERENCES content (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE catalog ADD CONSTRAINT FK_1B2C3247727ACA70 FOREIGN KEY (parent_id) REFERENCES catalog (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE related_catalog ADD CONSTRAINT FK_2435F8E2CC3C66FC FOREIGN KEY (catalog_id) REFERENCES catalog (id)');
        $this->addSql('ALTER TABLE related_catalog ADD CONSTRAINT FK_2435F8E24162C001 FOREIGN KEY (related_id) REFERENCES catalog (id)');

        $this->addSql('ALTER TABLE catalog_meta ADD CONSTRAINT FK_A247A443CC3C66FC FOREIGN KEY (catalog_id) REFERENCES catalog (id)');
        $this->addSql('ALTER TABLE catalog_project ADD CONSTRAINT FK_20012DC2166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE catalog_project ADD CONSTRAINT FK_20012DC2CC3C66FC FOREIGN KEY (catalog_id) REFERENCES catalog (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1727ACA70 FOREIGN KEY (parent_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_groups ADD CONSTRAINT FK_FF85F30B12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE category_groups ADD CONSTRAINT FK_FF85F30BFE54D947 FOREIGN KEY (group_id) REFERENCES category_group_parameters (id)');
        $this->addSql('ALTER TABLE content ADD CONSTRAINT FK_FEC530A912469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE content ADD CONSTRAINT FK_FEC530A9166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');

        $this->addSql('ALTER TABLE related_content ADD CONSTRAINT FK_C1DCFA0C84A0A3ED FOREIGN KEY (content_id) REFERENCES content (id)');
        $this->addSql('ALTER TABLE related_content ADD CONSTRAINT FK_C1DCFA0C4162C001 FOREIGN KEY (related_id) REFERENCES content (id)');

        $this->addSql('ALTER TABLE content_parameter ADD CONSTRAINT FK_1A70D91384A0A3ED FOREIGN KEY (content_id) REFERENCES content (id)');
        $this->addSql('ALTER TABLE content_parameter ADD CONSTRAINT FK_1A70D9137C56DBD6 FOREIGN KEY (parameter_id) REFERENCES category_parameter (id)');

        $this->addSql('ALTER TABLE tag_content ADD CONSTRAINT FK_CCF41D0384A0A3ED FOREIGN KEY (content_id) REFERENCES content (id)');
        $this->addSql('ALTER TABLE tag_content ADD CONSTRAINT FK_CCF41D03BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id)');

        $this->addSql('ALTER TABLE content_meta ADD CONSTRAINT FK_1A90F9F084A0A3ED FOREIGN KEY (content_id) REFERENCES content (id)');

        $this->addSql('ALTER TABLE order_content ADD CONSTRAINT FK_8BF99E284A0A3ED FOREIGN KEY (content_id) REFERENCES content (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE order_content ADD CONSTRAINT FK_8BF99E28D9F6D38 FOREIGN KEY (order_id) REFERENCES wf_order (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE catalog_sale ADD CONSTRAINT FK_90FE707384A0A3ED FOREIGN KEY (content_id) REFERENCES content (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_546390C8E16C6B94 ON category_group_parameters (alias)');
        $this->addSql('ALTER TABLE wf_order ADD CONSTRAINT FK_E502E098A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE wf_order ADD CONSTRAINT FK_E502E098166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE wf_order ADD CONSTRAINT FK_E502E098DCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_parameter ADD CONSTRAINT FK_EDF74320FE54D947 FOREIGN KEY (group_id) REFERENCES category_group_parameters (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_field ADD CONSTRAINT FK_48A04CC6166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_field ADD CONSTRAINT FK_48A04CC679798C6B FOREIGN KEY (project_field_id) REFERENCES field (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stock_content ADD CONSTRAINT FK_E763200A84A0A3ED FOREIGN KEY (content_id) REFERENCES catalog_sale (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stock_content ADD CONSTRAINT FK_E763200ADCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tag ADD CONSTRAINT FK_389B783166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE catalog_content (content_id INT NOT NULL, catalog_id INT NOT NULL, INDEX IDX_F177CD8584A0A3ED (content_id), INDEX IDX_F177CD85CC3C66FC (catalog_id), PRIMARY KEY(content_id, catalog_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE catalog_parameter (catalog_id INT NOT NULL, parameter_id INT NOT NULL, INDEX IDX_D0AA7BADCC3C66FC (catalog_id), INDEX IDX_D0AA7BAD7C56DBD6 (parameter_id), PRIMARY KEY(catalog_id, parameter_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE catalog_stock (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ext_log_entries (id INT AUTO_INCREMENT NOT NULL, action VARCHAR(8) NOT NULL COLLATE utf8_unicode_ci, logged_at DATETIME NOT NULL, object_id VARCHAR(64) DEFAULT NULL COLLATE utf8_unicode_ci, object_class VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, version INT NOT NULL, data LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:array)\', username VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, INDEX log_class_lookup_idx (object_class), INDEX log_date_lookup_idx (logged_at), INDEX log_user_lookup_idx (username), INDEX log_version_lookup_idx (object_id, object_class, version), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ext_translations (id INT AUTO_INCREMENT NOT NULL, locale VARCHAR(8) NOT NULL COLLATE utf8_unicode_ci, object_class VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, field VARCHAR(32) NOT NULL COLLATE utf8_unicode_ci, foreign_key VARCHAR(64) NOT NULL COLLATE utf8_unicode_ci, content LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX lookup_unique_idx (locale, object_class, field, foreign_key), INDEX translations_lookup_idx (locale, object_class, foreign_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE file_content (content_id INT NOT NULL, file_id INT NOT NULL, INDEX IDX_A5A3899284A0A3ED (content_id), INDEX IDX_A5A3899293CB796C (file_id), PRIMARY KEY(content_id, file_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE file_gallery (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, path VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, origin_name VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, sort INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, is_active TINYINT(1) DEFAULT NULL, project_id INT DEFAULT NULL, INDEX IDX_1C4DC10112469DE2 (category_id), INDEX IDX_1C4DC101166D1F9C (project_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE file_gallery_category (id INT AUTO_INCREMENT NOT NULL, sort INT DEFAULT NULL, alias VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, title VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, is_active TINYINT(1) DEFAULT NULL, project_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_D833D912E16C6B94 (alias), INDEX IDX_D833D912166D1F9C (project_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE file_gallery_content (content_id INT NOT NULL, file_gallery_id INT NOT NULL, INDEX IDX_3A9B264484A0A3ED (content_id), INDEX IDX_3A9B2644BD0F7439 (file_gallery_id), PRIMARY KEY(content_id, file_gallery_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, roles LONGTEXT NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_4B019DDB5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE image_gallery (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, path VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, sort INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, is_active TINYINT(1) DEFAULT NULL, image_origin_name VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, alt VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, INDEX IDX_D23B2FE612469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE image_gallery_category (id INT AUTO_INCREMENT NOT NULL, sort INT DEFAULT NULL, alias VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, title VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, is_active TINYINT(1) DEFAULT NULL, project_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_B46498B6E16C6B94 (alias), INDEX IDX_B46498B6166D1F9C (project_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE image_gallery_content (content_id INT NOT NULL, image_gallery_id INT NOT NULL, INDEX IDX_6B4F6F1084A0A3ED (content_id), INDEX IDX_6B4F6F106839B8B9 (image_gallery_id), PRIMARY KEY(content_id, image_gallery_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `order` (id INT AUTO_INCREMENT NOT NULL, stock_id INT DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, total_price VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, delivery VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, payment VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, status VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, created DATETIME NOT NULL, updated DATETIME NOT NULL, INDEX IDX_F5299398DCD6110 (stock_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_project (user_id INT NOT NULL, project_id INT NOT NULL, INDEX IDX_77BECEE4A76ED395 (user_id), INDEX IDX_77BECEE4166D1F9C (project_id), PRIMARY KEY(user_id, project_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE video_gallery (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, url_video VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, path VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, sort INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, is_active TINYINT(1) DEFAULT NULL, INDEX IDX_A2C6919712469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE video_gallery_category (id INT AUTO_INCREMENT NOT NULL, sort INT DEFAULT NULL, alias VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, title VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, is_active TINYINT(1) DEFAULT NULL, project_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_9E4BDD6DE16C6B94 (alias), INDEX IDX_9E4BDD6D166D1F9C (project_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE video_gallery_content (content_id INT NOT NULL, video_gallery_id INT NOT NULL, INDEX IDX_2B2120EB84A0A3ED (content_id), INDEX IDX_2B2120EB8440B739 (video_gallery_id), PRIMARY KEY(content_id, video_gallery_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wf_order_content (content_id INT NOT NULL, order_id INT NOT NULL, quantity INT NOT NULL, price NUMERIC(8, 2) NOT NULL, discount INT DEFAULT NULL, weight INT DEFAULT NULL, length INT DEFAULT NULL, width INT DEFAULT NULL, height INT DEFAULT NULL, barcode VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, INDEX IDX_D8B4312384A0A3ED (content_id), INDEX IDX_D8B431238D9F6D38 (order_id), PRIMARY KEY(content_id, order_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wf_order_goods (id INT AUTO_INCREMENT NOT NULL, order_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, alias VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, article VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, price DOUBLE PRECISION NOT NULL, discount INT NOT NULL, count INT NOT NULL, image_path VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, title_image VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, alt_image VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, goods_id INT NOT NULL, INDEX IDX_539ADC808D9F6D38 (order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE catalog DROP FOREIGN KEY FK_1B2C324784A0A3ED');
        $this->addSql('ALTER TABLE catalog DROP FOREIGN KEY FK_1B2C3247727ACA70');
        $this->addSql('ALTER TABLE catalog_meta DROP FOREIGN KEY FK_A247A443CC3C66FC');
        $this->addSql('ALTER TABLE catalog_project DROP FOREIGN KEY FK_20012DC2166D1F9C');
        $this->addSql('ALTER TABLE catalog_project DROP FOREIGN KEY FK_20012DC2CC3C66FC');
        $this->addSql('ALTER TABLE catalog_sale DROP FOREIGN KEY FK_90FE707384A0A3ED');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1727ACA70');
        $this->addSql('DROP INDEX UNIQ_546390C8E16C6B94 ON category_group_parameters');
        $this->addSql('ALTER TABLE category_groups DROP FOREIGN KEY FK_FF85F30B12469DE2');
        $this->addSql('ALTER TABLE category_groups DROP FOREIGN KEY FK_FF85F30BFE54D947');
        $this->addSql('ALTER TABLE category_parameter DROP FOREIGN KEY FK_EDF74320FE54D947');
        $this->addSql('ALTER TABLE content DROP FOREIGN KEY FK_FEC530A912469DE2');
        $this->addSql('ALTER TABLE content DROP FOREIGN KEY FK_FEC530A9166D1F9C');
        $this->addSql('ALTER TABLE content_meta DROP FOREIGN KEY FK_1A90F9F084A0A3ED');
        $this->addSql('ALTER TABLE content_parameter DROP FOREIGN KEY FK_1A70D91384A0A3ED');
        $this->addSql('ALTER TABLE content_parameter DROP FOREIGN KEY FK_1A70D9137C56DBD6');
        $this->addSql('ALTER TABLE order_content DROP FOREIGN KEY FK_8BF99E284A0A3ED');
        $this->addSql('ALTER TABLE order_content DROP FOREIGN KEY FK_8BF99E28D9F6D38');
        $this->addSql('ALTER TABLE project_field DROP FOREIGN KEY FK_48A04CC6166D1F9C');
        $this->addSql('ALTER TABLE project_field DROP FOREIGN KEY FK_48A04CC679798C6B');
        $this->addSql('ALTER TABLE related_catalog DROP FOREIGN KEY FK_2435F8E2CC3C66FC');
        $this->addSql('ALTER TABLE related_catalog DROP FOREIGN KEY FK_2435F8E24162C001');
        $this->addSql('ALTER TABLE related_content DROP FOREIGN KEY FK_C1DCFA0C84A0A3ED');
        $this->addSql('ALTER TABLE related_content DROP FOREIGN KEY FK_C1DCFA0C4162C001');
        $this->addSql('ALTER TABLE stock_content DROP FOREIGN KEY FK_E763200A84A0A3ED');
        $this->addSql('ALTER TABLE stock_content DROP FOREIGN KEY FK_E763200ADCD6110');
        $this->addSql('ALTER TABLE tag DROP FOREIGN KEY FK_389B783166D1F9C');
        $this->addSql('ALTER TABLE tag_content DROP FOREIGN KEY FK_CCF41D0384A0A3ED');
        $this->addSql('ALTER TABLE tag_content DROP FOREIGN KEY FK_CCF41D03BAD26311');
        $this->addSql('ALTER TABLE wf_order DROP FOREIGN KEY FK_E502E098A76ED395');
        $this->addSql('ALTER TABLE wf_order DROP FOREIGN KEY FK_E502E098166D1F9C');
        $this->addSql('ALTER TABLE wf_order DROP FOREIGN KEY FK_E502E098DCD6110');
    }
}
