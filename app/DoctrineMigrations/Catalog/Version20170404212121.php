<?php

namespace Application\Migrations\Catalog;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170404212121 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

//        $this->addSql('CREATE UNIQUE INDEX UNIQ_546390C8E16C6B94 ON category_group_parameters (alias)');
        $this->addSql('ALTER TABLE fos_user ADD address VARCHAR(225) DEFAULT NULL, ADD messenger VARCHAR(100) DEFAULT NULL, CHANGE phone phone VARCHAR(30) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

//        $this->addSql('DROP INDEX UNIQ_546390C8E16C6B94 ON category_group_parameters');
        $this->addSql('ALTER TABLE fos_user DROP address, DROP messenger, CHANGE phone phone VARCHAR(225) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
