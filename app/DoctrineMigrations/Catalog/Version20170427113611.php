<?php

namespace Application\Migrations\Catalog;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170427113611 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');


        $this->addSql('ALTER TABLE order_content DROP weight, DROP length, DROP width, DROP height, DROP barcode');
        $this->addSql('ALTER TABLE wf_order DROP country, DROP region, DROP city, DROP street, DROP house, DROP room, DROP postcode, DROP phone, DROP email');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_content ADD weight INT DEFAULT NULL, ADD length INT DEFAULT NULL, ADD width INT DEFAULT NULL, ADD height INT DEFAULT NULL, ADD barcode VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE wf_order ADD country VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD region VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD city VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD street VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD house VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD room VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD postcode VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, ADD phone VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD email VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
