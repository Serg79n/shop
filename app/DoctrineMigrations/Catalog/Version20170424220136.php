<?php

namespace Application\Migrations\Catalog;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170424220136 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

         $this->addSql('ALTER TABLE order_content DROP FOREIGN KEY FK_8BF99E284A0A3ED');
        $this->addSql('ALTER TABLE order_content DROP FOREIGN KEY FK_8BF99E28D9F6D38');
        $this->addSql('ALTER TABLE order_content DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE order_content ADD comment VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE order_content ADD CONSTRAINT FK_8BF99E284A0A3ED FOREIGN KEY (content_id) REFERENCES content (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE order_content ADD CONSTRAINT FK_8BF99E28D9F6D38 FOREIGN KEY (order_id) REFERENCES wf_order (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE order_content ADD PRIMARY KEY (content_id, order_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_content DROP FOREIGN KEY FK_8BF99E284A0A3ED');
        $this->addSql('ALTER TABLE order_content DROP FOREIGN KEY FK_8BF99E28D9F6D38');
        $this->addSql('ALTER TABLE order_content DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE order_content DROP comment');
        $this->addSql('ALTER TABLE order_content ADD CONSTRAINT FK_8BF99E284A0A3ED FOREIGN KEY (content_id) REFERENCES content (id)');
        $this->addSql('ALTER TABLE order_content ADD CONSTRAINT FK_8BF99E28D9F6D38 FOREIGN KEY (order_id) REFERENCES wf_order (id)');
        $this->addSql('ALTER TABLE order_content ADD PRIMARY KEY (order_id, content_id)');
    }
}
