<?php

namespace Application\Migrations\Content;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170116103943 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE pz_section (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(1000) NOT NULL, lft INT NOT NULL, rgt INT NOT NULL, root INT DEFAULT NULL, lvl INT NOT NULL, INDEX IDX_4C57E05E727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pz_section ADD CONSTRAINT FK_4C57E05E727ACA70 FOREIGN KEY (parent_id) REFERENCES pz_section (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pz_content ADD section_id INT DEFAULT NULL, ADD created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE pz_content ADD CONSTRAINT FK_9FE1AA18D823E37A FOREIGN KEY (section_id) REFERENCES pz_section (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_9FE1AA18D823E37A ON pz_content (section_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pz_content DROP FOREIGN KEY FK_9FE1AA18D823E37A');
        $this->addSql('ALTER TABLE pz_section DROP FOREIGN KEY FK_4C57E05E727ACA70');
        $this->addSql('DROP TABLE pz_section');
        $this->addSql('DROP INDEX IDX_9FE1AA18D823E37A ON pz_content');
        $this->addSql('ALTER TABLE pz_content DROP section_id, DROP created_at');
    }
}
