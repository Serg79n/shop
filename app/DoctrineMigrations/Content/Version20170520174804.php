<?php

namespace Application\Migrations\Content;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170520174804 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE pz_form_user (user_id INT NOT NULL, form_id INT NOT NULL, INDEX IDX_F4B656D9A76ED395 (user_id), INDEX IDX_F4B656D95FF69B7D (form_id), PRIMARY KEY(user_id, form_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pz_event (id INT AUTO_INCREMENT NOT NULL, alias VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, subject VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, is_active TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pz_field (id INT AUTO_INCREMENT NOT NULL, alias VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, is_active TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pz_form (id INT AUTO_INCREMENT NOT NULL, form_template_id INT DEFAULT NULL, alias VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, mail_to LONGTEXT DEFAULT NULL, domains LONGTEXT DEFAULT NULL, INDEX IDX_12325BBBF2B19FA9 (form_template_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pz_form_form_event (form_id INT NOT NULL, event_id INT NOT NULL, INDEX IDX_D8A504665FF69B7D (form_id), INDEX IDX_D8A5046671F7E88B (event_id), PRIMARY KEY(form_id, event_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pz_form_comment (id INT AUTO_INCREMENT NOT NULL, form_value_id INT DEFAULT NULL, user_id INT DEFAULT NULL, comment LONGTEXT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_19E0A6E9481A3EE6 (form_value_id), INDEX IDX_19E0A6E9A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pz_form_event (id INT AUTO_INCREMENT NOT NULL, form_id INT DEFAULT NULL, event_id INT DEFAULT NULL, subject VARCHAR(255) DEFAULT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_CBD8BC635FF69B7D (form_id), INDEX IDX_CBD8BC6371F7E88B (event_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pz_form_field (id INT AUTO_INCREMENT NOT NULL, form_id INT DEFAULT NULL, form_field_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, class VARCHAR(255) DEFAULT NULL, placeholder VARCHAR(255) DEFAULT NULL, attr VARCHAR(255) DEFAULT NULL, is_required TINYINT(1) DEFAULT NULL, regexp_pattern VARCHAR(255) DEFAULT NULL, alias VARCHAR(100) NOT NULL, sort INT DEFAULT NULL, default_value LONGTEXT DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, INDEX IDX_AB83F39C5FF69B7D (form_id), INDEX IDX_AB83F39CF50D82F4 (form_field_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pz_form_status (id INT AUTO_INCREMENT NOT NULL, form_value_id INT DEFAULT NULL, status_id INT DEFAULT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_E7F91503481A3EE6 (form_value_id), INDEX IDX_E7F915036BF700BD (status_id), INDEX IDX_E7F91503A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pz_form_template (id INT AUTO_INCREMENT NOT NULL, form_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, template LONGTEXT NOT NULL, css LONGTEXT DEFAULT NULL, js LONGTEXT DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, url VARCHAR(1000) DEFAULT NULL, INDEX IDX_A3FFCD85FF69B7D (form_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pz_form_value (id INT AUTO_INCREMENT NOT NULL, form_id INT DEFAULT NULL, created_at DATETIME NOT NULL, is_active TINYINT(1) DEFAULT NULL, more_field LONGTEXT DEFAULT NULL, INDEX IDX_ED01EEF05FF69B7D (form_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pz_status (id INT AUTO_INCREMENT NOT NULL, alias VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, sort INT DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pz_template (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, template LONGTEXT NOT NULL, css LONGTEXT DEFAULT NULL, js LONGTEXT DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pz_form_user ADD CONSTRAINT FK_F4B656D9A76ED395 FOREIGN KEY (user_id) REFERENCES pz_account (id)');
        $this->addSql('ALTER TABLE pz_form_user ADD CONSTRAINT FK_F4B656D95FF69B7D FOREIGN KEY (form_id) REFERENCES pz_form (id)');
        $this->addSql('ALTER TABLE pz_form ADD CONSTRAINT FK_12325BBBF2B19FA9 FOREIGN KEY (form_template_id) REFERENCES pz_form_template (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE pz_form_form_event ADD CONSTRAINT FK_D8A504665FF69B7D FOREIGN KEY (form_id) REFERENCES pz_form (id)');
        $this->addSql('ALTER TABLE pz_form_form_event ADD CONSTRAINT FK_D8A5046671F7E88B FOREIGN KEY (event_id) REFERENCES pz_event (id)');
        $this->addSql('ALTER TABLE pz_form_comment ADD CONSTRAINT FK_19E0A6E9481A3EE6 FOREIGN KEY (form_value_id) REFERENCES pz_form_value (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pz_form_comment ADD CONSTRAINT FK_19E0A6E9A76ED395 FOREIGN KEY (user_id) REFERENCES pz_account (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pz_form_event ADD CONSTRAINT FK_CBD8BC635FF69B7D FOREIGN KEY (form_id) REFERENCES pz_form (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pz_form_event ADD CONSTRAINT FK_CBD8BC6371F7E88B FOREIGN KEY (event_id) REFERENCES pz_event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pz_form_field ADD CONSTRAINT FK_AB83F39C5FF69B7D FOREIGN KEY (form_id) REFERENCES pz_form (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pz_form_field ADD CONSTRAINT FK_AB83F39CF50D82F4 FOREIGN KEY (form_field_id) REFERENCES pz_field (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pz_form_status ADD CONSTRAINT FK_E7F91503481A3EE6 FOREIGN KEY (form_value_id) REFERENCES pz_form_value (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pz_form_status ADD CONSTRAINT FK_E7F915036BF700BD FOREIGN KEY (status_id) REFERENCES pz_status (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pz_form_status ADD CONSTRAINT FK_E7F91503A76ED395 FOREIGN KEY (user_id) REFERENCES pz_account (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pz_form_template ADD CONSTRAINT FK_A3FFCD85FF69B7D FOREIGN KEY (form_id) REFERENCES pz_form (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pz_form_value ADD CONSTRAINT FK_ED01EEF05FF69B7D FOREIGN KEY (form_id) REFERENCES pz_form (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pz_form_form_event DROP FOREIGN KEY FK_D8A5046671F7E88B');
        $this->addSql('ALTER TABLE pz_form_event DROP FOREIGN KEY FK_CBD8BC6371F7E88B');
        $this->addSql('ALTER TABLE pz_form_field DROP FOREIGN KEY FK_AB83F39CF50D82F4');
        $this->addSql('ALTER TABLE pz_form_user DROP FOREIGN KEY FK_F4B656D95FF69B7D');
        $this->addSql('ALTER TABLE pz_form_form_event DROP FOREIGN KEY FK_D8A504665FF69B7D');
        $this->addSql('ALTER TABLE pz_form_event DROP FOREIGN KEY FK_CBD8BC635FF69B7D');
        $this->addSql('ALTER TABLE pz_form_field DROP FOREIGN KEY FK_AB83F39C5FF69B7D');
        $this->addSql('ALTER TABLE pz_form_template DROP FOREIGN KEY FK_A3FFCD85FF69B7D');
        $this->addSql('ALTER TABLE pz_form_value DROP FOREIGN KEY FK_ED01EEF05FF69B7D');
        $this->addSql('ALTER TABLE pz_form DROP FOREIGN KEY FK_12325BBBF2B19FA9');
        $this->addSql('ALTER TABLE pz_form_comment DROP FOREIGN KEY FK_19E0A6E9481A3EE6');
        $this->addSql('ALTER TABLE pz_form_status DROP FOREIGN KEY FK_E7F91503481A3EE6');
        $this->addSql('ALTER TABLE pz_form_status DROP FOREIGN KEY FK_E7F915036BF700BD');
        $this->addSql('DROP TABLE pz_form_user');
        $this->addSql('DROP TABLE pz_event');
        $this->addSql('DROP TABLE pz_field');
        $this->addSql('DROP TABLE pz_form');
        $this->addSql('DROP TABLE pz_form_form_event');
        $this->addSql('DROP TABLE pz_form_comment');
        $this->addSql('DROP TABLE pz_form_event');
        $this->addSql('DROP TABLE pz_form_field');
        $this->addSql('DROP TABLE pz_form_status');
        $this->addSql('DROP TABLE pz_form_template');
        $this->addSql('DROP TABLE pz_form_value');
        $this->addSql('DROP TABLE pz_status');
        $this->addSql('DROP TABLE pz_template');
    }
}
