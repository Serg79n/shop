<?php

namespace Application\Migrations\Content;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170122072706 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE pz_menu (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, alias VARCHAR(100) NOT NULL, is_active TINYINT(1) DEFAULT NULL, UNIQUE INDEX UNIQ_3DBF9C675E237E06 (name), UNIQUE INDEX UNIQ_3DBF9C67E16C6B94 (alias), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pz_menu_item (id INT AUTO_INCREMENT NOT NULL, menu_id INT NOT NULL, parent_id INT DEFAULT NULL, title VARCHAR(100) DEFAULT NULL, alias VARCHAR(100) NOT NULL, link VARCHAR(250) NOT NULL, is_active TINYINT(1) DEFAULT NULL, lft INT NOT NULL, rgt INT NOT NULL, root INT DEFAULT NULL, lvl INT NOT NULL, UNIQUE INDEX UNIQ_3A5051D7E16C6B94 (alias), INDEX IDX_3A5051D7CCD7E912 (menu_id), INDEX IDX_3A5051D7727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pz_menu_item ADD CONSTRAINT FK_3A5051D7CCD7E912 FOREIGN KEY (menu_id) REFERENCES pz_menu (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pz_menu_item ADD CONSTRAINT FK_3A5051D7727ACA70 FOREIGN KEY (parent_id) REFERENCES pz_menu_item (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pz_menu_item DROP FOREIGN KEY FK_3A5051D7CCD7E912');
        $this->addSql('ALTER TABLE pz_menu_item DROP FOREIGN KEY FK_3A5051D7727ACA70');
        $this->addSql('DROP TABLE pz_menu');
        $this->addSql('DROP TABLE pz_menu_item');
    }
}
