<?php

namespace Application\Migrations\Content;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170115173726 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE pz_content (id INT AUTO_INCREMENT NOT NULL, layout_id INT DEFAULT NULL, title VARCHAR(100) DEFAULT NULL, alias VARCHAR(100) NOT NULL, anons LONGTEXT DEFAULT NULL, show_editor_anons TINYINT(1) DEFAULT NULL, image_path VARCHAR(255) DEFAULT NULL, image_origin_name VARCHAR(100) DEFAULT NULL, content LONGTEXT DEFAULT NULL, show_editor_content TINYINT(1) DEFAULT NULL, big_image_path VARCHAR(255) DEFAULT NULL, big_image_origin_name VARCHAR(100) DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, UNIQUE INDEX UNIQ_9FE1AA18E16C6B94 (alias), INDEX IDX_9FE1AA188C22AA1A (layout_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pz_content_meta (id INT AUTO_INCREMENT NOT NULL, content_id INT DEFAULT NULL, meta_title VARCHAR(255) DEFAULT NULL, meta_keywords VARCHAR(255) DEFAULT NULL, meta_description LONGTEXT DEFAULT NULL, more_scripts LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_A6686D4E84A0A3ED (content_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pz_layout (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(1000) NOT NULL, file VARCHAR(255) NOT NULL, is_active TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pz_content ADD CONSTRAINT FK_9FE1AA188C22AA1A FOREIGN KEY (layout_id) REFERENCES pz_layout (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pz_content_meta ADD CONSTRAINT FK_A6686D4E84A0A3ED FOREIGN KEY (content_id) REFERENCES pz_content (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pz_content_meta DROP FOREIGN KEY FK_A6686D4E84A0A3ED');
        $this->addSql('ALTER TABLE pz_content DROP FOREIGN KEY FK_9FE1AA188C22AA1A');
        $this->addSql('DROP TABLE pz_content');
        $this->addSql('DROP TABLE pz_content_meta');
        $this->addSql('DROP TABLE pz_layout');
    }
}
